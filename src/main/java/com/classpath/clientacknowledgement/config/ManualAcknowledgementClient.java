package com.classpath.clientacknowledgement.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.StaticMessageHeaderAccessor;
import org.springframework.integration.acks.AckUtils;
import org.springframework.integration.acks.AcknowledgmentCallback;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
@Slf4j
public class ManualAcknowledgementClient {

    @Bean
    public Function<Message<String>, String> processor(){
        return (payload) -> {
            final AcknowledgmentCallback acknowledgmentCallback = StaticMessageHeaderAccessor.getAcknowledgmentCallback(payload);
            //connection.setAutoCommit(false);
            acknowledgmentCallback.noAutoAck();
            final Object authorizationKey = payload.getHeaders().get("authorization");
            try {
                if (authorizationKey.equals("1234")) {
                    System.out.println(" Acknowledge the message");
                    //statement.commit();
                    AckUtils.accept(acknowledgmentCallback);
                    return "Message successfully proceessed";
                } else {
                    System.out.println("Reque the message");
                    AckUtils.requeue(acknowledgmentCallback);
                    //statement.rollback();
                    return "Invalid message";
                }
            }catch (Exception exception){
              log.error("Message could not be processed :: " + exception.getMessage());
            }
            return "default message";
        };
    }

    @Bean
    public Supplier<Message<String>> publishMessage() {
        return () -> {
            final Message<String> message = MessageBuilder
                    .withPayload(UUID.randomUUID().toString())
                    .setHeader("authorization", "88889")
                    .build();
            return  message;
        };
    }
}