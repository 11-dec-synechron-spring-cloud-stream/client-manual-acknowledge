package com.classpath.clientacknowledgement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientAcknowledgementApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientAcknowledgementApplication.class, args);
    }

}
